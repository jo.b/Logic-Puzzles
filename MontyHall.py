################################################
#
#  Short program that tests out a variation of the monty hall problem I was curious about, as well as the original for
#  some context.
#
#  Classic - After you guess a door, monty opens (total doors - 2) doors that definitely do contain goats and were not
#            your choice.  You pick if you switch your door to the only other remaining door or stay.
#  Variation - After you guess a door, monty opens a random door, if monty's door contains the car, you lose!  If not,
#            you get to choose if you pick another door randomly, always pick another one, or never pick another one.
#            Game goes on in this manner until the last two doors are picked and you either end up with the car or not.
#
#  Note: just hacked this together, could definitely be better written
################################################


import numpy as np

if __name__ == '__main__':

    # Get user input

    classic = int(input("Use classic Monty Hall? 1: Yes, 2: No :"))
    switch = int(input("1: Switch randomly, 2: Always switch, 3: Never switch : "))
    door_count = int(input("Enter total number of doors: "))
    attempts = int(input("Enter the number of times to run this test: "))

    win_total = 0

    for i in range(0,attempts):

        # Generate a field
        field = list("G" * door_count)
        field[np.random.randint(0, door_count - 1)] = "C"

        # Generate pickable index list
        pickable = [i for i in range(0, door_count)]

        # Make first choice
        your_choice_last = np.random.choice(pickable, 1)

        # Classic Monty Hall
        if classic == 1:
            # Change from 1 item list into just int
            your_choice_last = your_choice_last[0]

            # Remove your choice from doors monty picks from
            del pickable[pickable.index(your_choice_last)]

            definite_goats = []

            # Pick goats
            for i in range(0,door_count-2):
                if field[pickable[i]] == 'G':
                    definite_goats.append(pickable[i])

            # Find the index of the other door
            left_over = [item for item in pickable if item not in definite_goats][0]

            # Decide to switch doors or not
            if switch == 1:
                your_choice = np.random.choice([your_choice_last, left_over], 1)[0]
            elif switch == 2:
                your_choice = left_over
            elif switch == 3:
                your_choice = your_choice_last

            # Determine if you won
            if field[your_choice] == 'C':
                win = True
            else:
                win = False

        # Monty Hall variations
        else:
            end = False

            while not end:
                # Generate choices
                if switch == 1:
                    your_choice = np.random.choice(pickable, 1)
                elif switch == 2:
                    your_choice = np.random.choice([item for item in pickable if item not in your_choice_last], 1)
                elif switch == 3:
                    your_choice = your_choice_last

                their_choice = np.random.choice([item for item in pickable if item not in your_choice])

                # Check if end
                if field[their_choice] == "C":
                    end = True
                    win = False
                else:
                    # Remove that choice
                    del pickable[pickable.index(their_choice)]

                    # If nothing has been picked and you have the last choice, you win!
                    if len(pickable) == 1:
                        end = True
                        win = True

                # Update last choice
                your_choice_last = your_choice

        win_total += win

    print(f"Wins: {win_total} out of {attempts} attempts - {win_total/attempts}%")





